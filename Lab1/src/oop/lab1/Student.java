package oop.lab1;
//TODO Write class Javadoc
/**
 * Student is a person that have id
 * @author sanrasern 5710547247
 *
 */
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	/**
	 * Initialize a new Student object.
	 * @param name is the name of the new Student
	 * @param id is the id of Student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}
	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}
	//TODO Write equals
	public boolean equals(Object obj) {
		if(obj == null) return false;
		if(obj.getClass() != this.getClass()) return false;
		Student other = (Student) obj;
		if(id == other.id)
			return true;
		return false;
	}
}